import angular from 'angular';
import angularMeteor from 'angular-meteor';
import nokiaGo from '../imports/components/nokiago/nokiago';
import footer from '../imports/components/footer/footer';
import error from '../imports/components/error/error';
import test from '../imports/components/test/test';

angular.module('nokia-go-app', [
  angularMeteor,
  nokiaGo.name,
  footer.name,
  error.name,
  test.name
]);

function onReady() {
  angular.bootstrap(document, ['nokia-go-app']);
    if (window.cordova.logger) {
      window.cordova.logger.__onDeviceReady();
  } 
}

var handle = LaunchScreen.hold();
if (Meteor.isCordova) {
  angular.element(document).on('deviceready', onReady);

  var count = 2;
  var timer = setInterval(function() {
    count--;
    console.log('count:', count);
    if (count == 0){
      handle.release();
      clearInterval(timer);
    }
  }, 1000);

  document.addEventListener("backbutton", function() {
    // pass exitApp as callbacks to the switchOff method
    window.plugins.flashlight.switchOff(exitApp, exitApp);
  }, false);

  function exitApp() {
    navigator.app.exitApp();
  }

} else {
  angular.element(document).ready(onReady);
}