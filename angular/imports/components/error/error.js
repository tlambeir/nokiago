import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './error.html';
import ngAnimate from 'angular-animate';
import ngSanitize from 'angular-sanitize';

class errorCtrl {

    constructor($scope, $element) {
        this.element = $element;
        this.faded = true;
    }
    init(){
        var resisableElements = this.element.find('.error-wrapper');
        var headerElement = this.element.siblings('.header');
        resisableElements.height(4 / 3 * window.innerWidth);
        resisableElements.css({ top: headerElement.outerHeight()});
        this.faded = false;
    }
    continue(){
        this.faded = true;
        setTimeout(function () {
            this.callBack();
        }.bind(this), 300);
    }
}

export default angular.module('error', [
    angularMeteor,
    ngAnimate,
    ngSanitize
])
    .component('error', {
        templateUrl: 'imports/components/error/error.html',
        controller: ['$scope','$element', errorCtrl],
        bindings: {
            error: '=',
            callBack : '&'
        },
    });