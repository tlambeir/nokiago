import angular from 'angular';
import angularMeteor from 'angular-meteor';
import ngAnimate from 'angular-animate';
import template from './footer.html';
import file from '/imports/services/file/file';
import angularTouch from 'angular-touch';

class footerCtrl {

    constructor($scope, $element, $q, fileService) {
        $scope.viewModel(this);
        this.element = $element;
        this.images;
        this.$q =$q;
        this.fileService = fileService;
        this.index = 0;
        this.faded = true;
    }
    init(){
        var headerElement = this.element.siblings('.header');
        var footer = this.element.find('.footer');
        footer.height(headerElement.outerHeight());
        var carousel = this.element.find('.carousel');
        carousel.height(footer.height());
        var imageWrappers = carousel.find('.img-wrapper');
        imageWrappers.height(carousel.height()*0.8);
        imageWrappers.width(carousel.height()*0.8);
        var marginTop = carousel.height()*0.1;
        imageWrappers.css('margin-top',marginTop);
        var marginLeft = carousel.width()-4*carousel.height()*0.8;
        marginLeft = marginLeft / 5;
        imageWrappers.css('margin-left',marginLeft);
        this.fileService.getImages(this);
    }

    setIndex(index){
        if(index >= 0 && index < this.images.length){
            this.faded = false;
            this.index = index;
            setTimeout(function () {
                this.faded = true;
            }.bind(this), 100);
        }
    }

    setSelectedImage(selectedImage){
        this.selectedImage = selectedImage;
        this.fromGallery = true;
    }
}

export default angular.module('footer', [
    angularMeteor,
    file.name,
    angularTouch,
    ngAnimate
])
    .component('footer', {
        templateUrl: 'imports/components/footer/footer.html',
        controller: ['$scope','$element','$q','fileService', footerCtrl],
        bindings: {
            images: '=',
            selectedImage: '=',
            fromGallery: '='
        },
    });