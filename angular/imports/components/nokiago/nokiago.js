import angular from 'angular';
import angularMeteor from 'angular-meteor';
import ngAnimate from 'angular-animate';
import template from './nokiago.html';
import camera from '/imports/services/camera/camera';
import file from '/imports/services/file/file';
import data from '/imports/services/data/data';
import googleVision from '/imports/services/google-vision/google-vision';

class nokiaGoCtrl {
    constructor($scope, $element, $window, cameraService, fileService, dataService, googleVisionService) {
        window.screen.lockOrientation('portrait');
        $scope.viewModel(this);
        this.startOnPictureEventListener();
        this.element = $element;
        this.cameraService = cameraService;
        this.fileService = fileService;
        this.dataService = dataService;
        this.googleVisionService = googleVisionService;
        this.percentage;
        this.images;
        this.selectedImage;
        this.results;
        this.isSwitchedOn;
        this.selectedResult;
        this.selectedTrigger;
        this.fromGallery = false;
        this.infoShown = false;
        this.window=$window;
    }
    startOnPictureEventListener(){
        cordova.plugins.camerapreview.setOnPictureTakenHandler(function(result){
            this.fromGallery = false;
            this.loading = true;
            this.shutterHidden = true;
            if(device.platform == "Android"){
                this.fileService.getBase64fileFromPath(this, 'file://'+result[0]);
            } else if (device.platform == "iOS"){
                this.fileService.insert(result[1]);
                this.fileService.getBase64fileFromPath(this, result[1]);
            }
            this.fileService.getImages(this);
        }.bind(this));
    }

    toggleFlashLight(){
        window.plugins.flashlight.available(function(isAvailable) {
            if (isAvailable) {
                window.plugins.flashlight.toggle(function(succ){
                }, function(err){
                });
            } else {
                console.log("Camera is unavailable");
                alert("Flashlight not available on this device");
            }
        });
    }

    startVideo() {
        var headerElement = this.element.find('.header');
        this.cameraService.startVideo(false,false,true,{x: 0, y: headerElement.outerHeight(), width: window.innerWidth, height: window.innerHeight - 2 * headerElement.outerHeight()});
    }

    takePicture(){
        this.cameraService.takePicture();
    }

    resize(){
        var resisableElements = this.element.find('.selected-image, .button-go-wrapper, .info2, .result, .trigger, .wrapper-shutter');
        var headerElement = this.element.find('.header');
        resisableElements.height(window.innerHeight - 2 * headerElement.outerHeight());
        resisableElements.css({ top: headerElement.outerHeight()});
    }

    delete(){
        this.fileService.delete(this, this.selectedImage.url).then(function(){
            for (var i=0; i<this.images.length; i++) {
                if(this.images[i].base64==this.selectedImage.base64){
                    this.images.splice(i, 1);
                }
            }
            this.cancel();
        }.bind(this));
    }

    deleteAction(action){
        switch (action){
            case 'delete':
                this.delete();
                break;
            case 'open':
                this.deleteConfirmation = true;
                break;
            case 'cancel':
                this.deleteConfirmation = false;
                break;
        }
    }
    approve(){
        // get response from cloudvision
        this.loading = true;
        this.shutterHidden = true;
        var base64 = angular.copy(this.selectedImage.base64);
        delete this.selectedImage;
        this.googleVisionService.detectLabel(base64).then(function(response){
            var trigger = response.data.responses[0].labelAnnotations[0].description;
            if(response.data.responses.length > 0) {
                this.percentage = response.data.responses[0].labelAnnotations[0].score * 100;
                this.dataService.find(trigger).then(function (results) {
                    console.log(results);
                    if (results.length > 0) {
                        this.results = results;
                        this.selectedTrigger = this.results[0];
                        this.loading = false;
                    } else {
                        this.error = {
                            icon:'Error_Database.png',
                            message:'This is probably a ' + response.data.responses[0].labelAnnotations[0].description + '..<br>But it\'s not in our IOT database yet!<br>Please continue to check another object!',
                        };
                    }
                }.bind(this));
            } else {
                this.error = {
                    icon:'Error_Image.png',
                    message:'We could not recognize this image..<br>Please continue to check another object!',
                };
            }
        }.bind(this))
            .catch(function(error){
                console.log(error);
                this.error = {
                    icon:'Error_Connection.png',
                    message:'There seems to be something wrong..<br>Please check your connection and continue!',
                };
            }.bind(this));

    }
    backToTrigger(){
        this.selectedTrigger = this.selectedTriggerCache;
        delete this.selectedResult;
    }
    selectTrigger(trigger){
        this.selectedTrigger = trigger;
        delete this.selectedResult;
    }
    selectResult(result){
        this.selectedResult = result;
        this.selectedTriggerCache = angular.copy(this.selectedTrigger);
        delete this.selectedTrigger;
    }
    cancel(){
        delete this.selectedImage;
        delete this.results;
        delete this.selectedResult;
        delete this.selectedTrigger;
        delete this.error;
        this.shutterHidden = false;
        this.infoShown = false;
        this.loading = false;
        this.deleteConfirmation = false;
    }

    showInfo(){
        this.infoShown = true;
    }

    rotateIcon (elementID) {
        var $rota = $(elementID),
            degree = 0,
            timer,
            angle = 5,
            delay = 41,
            acceleration = 0.15,
            maxAngle = 180;
        function spinRight() {
            $rota.css({ transform: 'rotate(' + degree + 'deg)'});
            timer = setTimeout(function() {
                angle += acceleration;
                degree += angle;
                if(degree < maxAngle){
                    spinRight(); // loop it
                } else {
                    angle = 5;
                    maxAngle = 180 + Math.floor( Math.random() * 4 ) * 30;
                    spinLeft();
                }
            },delay);
        }
        function spinLeft() {
            $rota.css({ transform: 'rotate(' + degree + 'deg)'});
            timer = setTimeout(function() {
                angle += acceleration;
                degree -= angle;
                if(degree > 0){
                    spinLeft();
                } else {
                    angle = 5;
                    spinRight(); // loop it
                }
            },delay);
        }
        spinRight();    // run it!
    };
    onclick(link){
    	if (link.includes("://")){
    		this.window.open(link, '_blank', 'location=yes');
    	} else {
    		this.window.open("https://networks.nokia.com/innovation/iot"+link.replace(' ','-'), '_blank', 'location=yes');
    	}
    };
}

export default angular.module('nokiaGo', [
    angularMeteor,
    camera.name,
    file.name,
    data.name,
    googleVision.name,
    ngAnimate
])
    .component('nokiaGo', {
        templateUrl: 'imports/components/nokiago/nokiago.html',
        controller: ['$scope','$element','$window','cameraService','fileService','dataService','googleVisionService', nokiaGoCtrl]
    })
    .filter('trustAsHtml', ['$sce', function($sce){
        return function(html) {
            return $sce.trustAsHtml(html);
        };
    }]);