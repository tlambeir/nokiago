import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './test.html';
import camera from '/imports/services/camera/camera';

class testCtrl {
    constructor($scope, $element, cameraService) {
        this.test = "een zin";
        this.test = "een andere zin";
        cameraService.doSomething();
    }
}

export default angular.module('test', [
    angularMeteor,
    camera.name
])
    .component('test', {
        templateUrl: 'imports/components/test/test.html',
        controller: ['$scope','$element','cameraService', testCtrl]
    });