import angular from 'angular';
import angularMeteor from 'angular-meteor';

export default angular.module('camera', [
    angularMeteor
])
    .service('cameraService', function() {
        this.startVideo = function(tapEnabled,dragEnabled,toBack, rect) {
            var tapEnabled = tapEnabled; //enable tap take picture
            var dragEnabled = dragEnabled; //enable preview box drag across the screen
            var toBack = toBack; //send preview box to the back of the webview
            var rect = rect;
            cordova.plugins.camerapreview.startCamera(rect, "rear", tapEnabled, dragEnabled, toBack)
        };

        this.takePicture = function (options) {
            cordova.plugins.camerapreview.takePicture(options);
        }
    });