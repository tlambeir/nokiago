import angular from 'angular';
import angularMeteor from 'angular-meteor';

export default angular.module('data', [
    angularMeteor
])
    .service('dataService', ['$http', function($http) {
        this.find = function(trigger){
            return $http({
                method: 'GET',
                url: 'http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiago/api/triggers',
                params:{item:trigger}
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                console.log(response);
            });
        }
    }]);