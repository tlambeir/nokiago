import angular from 'angular';
import angularMeteor from 'angular-meteor';

export default angular.module('file', [
    angularMeteor
])
    .service('fileService', ['$q', function($q) {

        this.getBase64fileFromPath = function (context, path) {
            var deferred = $q.defer();
            resolveLocalFileSystemURL(path, function(fileEntry) {
                // no callback
                fileEntry.file(function(file) {
                    var reader = new FileReader();
                    reader.onloadend = function(event) {
                        var base64 = event.target.result;
                        context.selectedImage = {
                            base64:base64
                        };
                        context.approve();
                        deferred.resolve($q.all());
                    };
                    reader.readAsDataURL(file);
                },function(error) {
                    console.log('fileEntry error');
                    console.log(error);
                });
            },function(error) {
                console.log('resolveLocalFileSystemURL error');
                console.log(error);
            });
        };

        this.getImages = function(context){
            var deferred = $q.defer();
            if(device.platform == "Android"){
                resolveLocalFileSystemURL(cordova.file.dataDirectory ,
                    function (fileSystem) {
                        console.log('in fileSystem');
                        var reader = fileSystem.createReader();
                        reader.readEntries(
                            function (entries) {
                                console.log('in readEntries');
                                var images  = [];
                                var promises = [];
                                for (i=entries.length-1; i>=0; i--) {
                                    var row = entries[i];
                                    if(!row.isDirectory && row.nativeURL.split('_').pop()=='original.jpg'){
                                        var deferred2 = $q.defer();
                                        // We will draw the content of the clicked folder
                                        entries[i].file(function(file) {
                                            var reader = new FileReader();
                                            reader.onloadend = function(event) {
                                                var image = {
                                                    url:event.target._localURL,
                                                    base64:event.target.result
                                                };
                                                images.push(image);
                                                deferred2.resolve(image);
                                            };
                                            reader.readAsDataURL(file);
                                        },function(error) {
                                            console.log('fileEntry error');
                                            console.log(error);
                                        });
                                        promises.push(deferred2.promise);
                                    }
                                }
                                deferred.resolve($q.all(promises));
                                context.images =  images;
                            }.bind(context),
                            function (err) {
                                console.log(err);
                            }
                        );
                    }.bind(context), function (err) {
                        console.log(err);
                    }
                );
            } else if (device.platform == "iOS") {
                var storage = window.localStorage;
                var assetLinks = angular.fromJson(storage.getItem("photo")).reverse();
                var images = [];
                var promises = [];
                angular.forEach(assetLinks, function (assetLink) {
                    var deferred2 = $q.defer();
                    resolveLocalFileSystemURL(assetLink, function(fileEntry) {
                        // no callback
                        fileEntry.file(function(file) {
                            var reader = new FileReader();
                            reader.onloadend = function(event) {
                                var image = {
                                    url: assetLink,
                                    base64: event.target.result
                                };
                                deferred2.resolve(image);
                                images.push(image);
                            };
                            reader.readAsDataURL(file);
                        },function(error) {
                            console.log('fileEntry error');
                            console.log(error);
                        });
                    },function(error) {
                        console.log('resolveLocalFileSystemURL error');
                        console.log(error);
                    });
                    promises.push(deferred2.promise);
                });
                deferred.resolve($q.all(promises));
                context.images =  images;
            }

            return deferred.promise;
        };

        this.insert = function(assetLink){
            var storage = window.localStorage;
            var value =  storage.getItem("photo");
            if (value == undefined) {
                value = [];
            } else {
                value = angular.fromJson(value);
            }
            value.push(assetLink);
            storage.setItem("photo", angular.toJson(value));
        };


        this.delete = function(context, path){
            if(device.platform == "Android") {
                var filename = path.split('/').pop();
                var deferred = $q.defer();
                resolveLocalFileSystemURL(cordova.file.dataDirectory + filename, function (entry) {
                    entry.remove(function (entry) {
                            deferred.resolve(entry);
                        },
                        function (error) {
                            deferred.reject(error);
                        });
                });
                return deferred.promise;
            } else if (device.platform == "iOS") {
                var storage = window.localStorage;
                var assetLinks = angular.fromJson(storage.getItem("photo")).reverse();
                if(assetLinks.indexOf(path) >=0){

                    assetLinks.splice(assetLinks.indexOf(path),1);
                    storage.removeItem("photo");
                    storage.setItem("photo", angular.toJson(assetLinks));
                }
                delete context.selectedImage;
                this.getImages(context);
            }
        };

    }]);