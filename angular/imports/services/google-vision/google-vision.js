import angular from 'angular';
import angularMeteor from 'angular-meteor';

export default angular.module('googleVision', [
	angularMeteor])
.service('googleVisionService', ['$http', function($http){

	var keys = {
		android:'AIzaSyDDiSHWLP-KnrBpL3uN9jJzzqmYce-XyBU',
		ios:'AIzaSyD2vlyMyUBuw4Ulc5bicGGZahBoAISpSGw',
	};


	this.detectLabel = function(base64Content){
		var url = 'https://vision.googleapis.com/v1/images:annotate?key=' + (device.platform == "Android"? keys.android : keys.ios);
		//
		var postData = angular.toJson({
			requests: [
				{
					image: {
						content: base64Content.replace('data:image/jpeg;base64,', '')
					},
					features: [
						{
							type: "LABEL_DETECTION",
							maxResults: 5
						}
					]
				}
			]
		}, true);

		return $http({
			method: 'POST',
			url: url,
			data: postData,
			headers: {'Content-Type': 'application/json'}
		}).catch(function(error){
			return 'Unable to connect to cloudvision';
		});
	}
}]);