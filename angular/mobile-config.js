App.info({
    id: 'com.nokia.go',
    name: 'Nokia GO',
    description: 'A mobile iot tool by Nokia',
    author: 'Nokia Applied Research',
    email: 'support@nokia.com',
    website: 'http://nokia.com'
});

App.setPreference('HideKeyboardFormAccessoryBar', true);
App.setPreference('Orientation', 'portrait');
App.setPreference('Orientation', 'all', 'ios');

App.setPreference('SplashScreenBackgroundColor', '#124191');
App.setPreference('SplashMaintainAspectRatio', 'true');



App.icons({
    // iOS
    'iphone_2x': 'resources/icons/AppIcon_iphone_2x.png', //(120x120)
    'iphone_3x': 'resources/icons/AppIcon_iphone_3x.png', //(180x180)
    'ipad': 'resources/icons/AppIcon_ipad.png', //(76x76)
    'ipad_2x': 'resources/icons/AppIcon_ipad_2x.png', //(152x152)
    'ipad_pro': 'resources/icons/AppIcon_ipad_pro.png', //(167x167)
    'ios_settings': 'resources/icons/AppIcon_ios_settings.png', //(29x29)
    'ios_settings_2x': 'resources/icons/AppIcon_ios_settings_2x.png', // (58x58)
    'ios_settings_3x': 'resources/icons/AppIcon_ios_settings_3x.png', //  (87x87)
    'ios_spotlight': 'resources/icons/AppIcon_ios_spotlight.png', //  (40x40)
    'ios_spotlight_2x': 'resources/icons/AppIcon_ios_spotlight_2x.png', //  (80x80)
    'android_mdpi': 'resources/icons/Icon_48.png', //  (48x48)
    'android_hdpi': 'resources/icons/Icon_72.png', // (72x72)
    'android_xhdpi': 'resources/icons/Icon_96.png', //  (96x96)
    'android_xxhdpi': 'resources/icons/Icon_144.png', //  (144x144)
    'android_xxxhdpi': 'resources/icons/Icon_192.png'//  (192x192)
});

App.launchScreens({
    'iphone_2x': 'resources/splash/iphone_2x.png',
    'iphone5': 'resources/splash/iphone5.png',
    'iphone6': 'resources/splash/Splash_Gif.png',
    'iphone6p_portrait': 'resources/splash/iphone6.png',
    'ipad_portrait': 'resources/splash/ipad_portrait.png',
    'ipad_portrait_2x': 'resources/splash/ipad_portrait_2x.png',
    'android_ldpi_portrait': 'resources/splash/android_ldpi_portrait.png',
    'android_mdpi_portrait': 'resources/splash/android_mdpi_portrait.png',
    'android_hdpi_portrait': 'resources/splash/android_hdpi_portrait.png',
    'android_xhdpi_portrait': 'resources/splash/android_xhdpi_portrait.png',
    'android_xxhdpi_portrait': 'resources/splash/android_xxhdpi_portrait.png',
});

