**Download and install meteor**
https://www.meteor.com/install

**Initializing meteor**
* from nokiago/
* meteor create app
* move the .meteor folder to the nokiago/angular/.meteor
* delete the app/ folder

**Configure meteor**
* meteor npm install
* meteor remove blaze-html-templates
* meteor add angular-templates
* meteor npm install --save angular angular-meteor
* meteor npm install --save angular-animate angular-sanitize

**Add cordova plugins**
* meteor add cordova:cordova-plugin-camera-preview@https://github.com/vinhjune/cordova-plugin-camera-preview.git#81894c0f66dabefd9b942207c89d4b37d7486ed1
* meteor add cordova:cordova-plugin-file@4.2.0
* meteor add cordova:cordova-plugin-flashlight@https://github.com/vinhjune/Flashlight-PhoneGap-Plugin.git#2175f333ea850c3d608a7bdbb13fc46fc919edba
* meteor add cordova:cordova-plugin-screen-orientation@1.4.2
* meteor add cordova:cordova-plugin-device@1.1.2
* meteor add cordova:cordova-plugin-console@1.0.3
*meteor add cordova:cordova-plugin-wkwebview-engine@1.0.3


**Launching App is Browser**

* meteor run
* open localhost:3000 in browser

**Install mobile sdk and libraries**

https://guide.meteor.com/mobile.html
    
**Luanching the app on an android device connected via usb**

* meteor add-platform android (only once)


    if you get the Error: Cannot find module 'is-property' error 
    go to C:\Users\YOURUSERNAME\AppData\Local\.meteor\packages\meteor-tool\1.3.4_1\mt-os.windows.x86_32\dev_bundle\lib\node_modules\cordova-lib\node_modules\npm\node_modules\request\node_modules\har-validator\node_modules\is-my-json-valid
    and run meteor npm install is-property
    This is an issue with limited folder names in windows
    
* meteor run android-device --mobile-server yourLocalIp:3000
* you can also start an emulator: emulator -avd test -http-proxy http://138.203.144.56:8000 and use meteor run android

**IOS "to do"**

* run this only once to add ios capability:
meteor add-platform ios

* To open the project in Xcode (from there I can choose whether to run on an emulator or on an actual iOS device)
meteor run ios-device

* Optionally, to run the app on iPhone 6 Plus simulator:
meteor run ios

**Cloudvision**

*Android:* 
AIzaSyDDiSHWLP-KnrBpL3uN9jJzzqmYce-XyBU

*IOS*
AIzaSyD2vlyMyUBuw4Ulc5bicGGZahBoAISpSGw

*Browser*
AIzaSyBgJH3Q0p-hHe8tLFBWRXdN0pPCjVLsOno

**Bulding and signing the application for Android**

* meteor build ../prod --server http://adummyserver.com --debug
* create a nokia_go.keystore: keytool -genkey -v -keystore nokia_go.keystore -alias nokia_go -keyalg RSA -validity 10000
* sign the apk using: sign-apk ../prod/android/debug.apk