#!/bin/bash

jarsigner -verbose -keystore nokia_go.keystore -digestalg SHA1 "$1" nokia_go

echo ""
echo ""
echo "Checking if APK is verified..."
jarsigner -verify -verbose -certs "$1"