# NOKIAGO HTPP MVC BACKEND

A Simple http MVC backend that supports GET, POST, PUT and DELETE Methods.

====================

Start local test server with **php -S localhost:8000 routing.php** (routing.php will be replaced by mod_headers configuration)

**http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiago/api/triggers** will show an overview of all the triggers

**http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiago/api/triggers?item=automobile** will only fetch data for a single trigger

====================

Based on A Most Simple PHP MVC Beginners Tutorial BY Neil Rosenstech


## License

MIT License (MIT)

Copyright (c) <2013> <Neil Rosenstech>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
