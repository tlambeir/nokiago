<?php
  class Db {
    private static $instance = NULL;

    private function __construct() {}

    private function __clone() {}

    public static function getInstance() {
      if (!isset(self::$instance)) {
        $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
        self::$instance = new PDO('mysql:host=ec2-52-51-31-225.eu-west-1.compute.amazonaws.com;dbname=iot', 'aredu', 'aredu20!6', $pdo_options);
      }
      return self::$instance;
    }
  }
?>
