<?php
require_once("httpcontroller.php");
  class TriggersController extends httpController{

      function __construct($actionWith) {
          parent::__construct($actionWith);
          $this->model = new Trigger();
      }

      public function GET() {
          $array = $this->fetchAll();
          require_once('views/json.php');
      }

      public function POST() {
          $this->model->setName($this->post->item);
          $array = $this->save();
          require_once('views/json.php');
      }

      public function PUT() {
          $this->model->setId($this->post->id)
              ->setName($this->post->item);
          $array = $this->save();
          require_once('views/json.php');
      }
  }