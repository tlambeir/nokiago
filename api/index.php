<?php
  require_once('connection.php');

  //todo: move to .htaccess file
  header("Access-Control-Allow-Origin: *");
  header("Access-Control-Allow-Credentials", "true");
  header('Access-Control-Allow-Methods : GET, POST, OPTIONS, PUT, DELETE');
  header("Access-Control-Allow-Headers: X-Requested-With, Content-Type, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding");

  $route = explode('/', strtok($_SERVER['REQUEST_URI'],'?'));

  if (isset($route[3])) {
    $controller = $route[3];
  } else {
    echo "No route specified!";
  }

    if (isset($route[4])) {
        $actionWith = $route[4];
    } else {
        $actionWith = null;
    }

  $action = filter_input( INPUT_SERVER, 'REQUEST_METHOD' );

  require_once('routes.php');
