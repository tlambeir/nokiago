<?php
require_once("model.php");
require_once("usecase.php");
class Category extends Model{

    /**
     * @var string
     */
    public $table = "categories";

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $category_name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCategoryName()
    {
        return $this->category_name;
    }

    /**
     * @param string $category_name
     * @return $this
     */
    public function setCategoryName($category_name)
    {
        $this->category_name = $category_name;
        return $this;
    }

    /**
     * @return array
     */
    public function fetchAll() {
        $list = array();
        // we create a list of Post objects from the database results
        foreach($this->all() as $result) {
            $category = new Category();
            $category->setId($result['id'])
                ->setItem($result['category_name']);
            $list[] = $category;
        }

        return $list;
    }

    /**
     * @param $id
     * @return Category
     */
    public function FetchById($id) {
        $result = $this->find($id);
        $category = new Category();
        $category->setId($result['id'])
            ->setCategoryName($result['category_name']);
        return $category;
    }

    public function save(){
        if(!$this->id){
            $category = $this->insert(array(
                "category_name" => $this->category_name
            ));
        } else {
            $category = $this->update(array(
                "id" => $this->id,
                "category_name" => $this->category_name,
            ));
        }
        unset($category->table);
        return $category;
    }
}