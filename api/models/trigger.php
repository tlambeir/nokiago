<?php
require_once("model.php");
require_once("usecase.php");
class Trigger extends Model{

    /**
     * @var string
     */
    public $table = "triggers";

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $item;

    /**
     * @var array
     */
    public $usecases;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param string $item
     * @return $this
     */
    public function setItem($item)
    {
        $this->item = $item;
        return $this;
    }

    /**
     * @return array
     */
    public function getUsecases()
    {
        return $this->usecases;
    }

    /**
     * @param array $usecases
     * @return $this
     */
    public function setUsecases($usecases)
    {
        $this->usecases = $usecases;
        return $this;
    }

    /**
     * @return array
     */
    public function fetchAll() {
        $list = array();
        // we create a list of Post objects from the database results
        foreach($this->all() as $result) {
            $trigger = new Trigger();
            $trigger->setId($result['id'])
                ->setItem($result['item']);

            //find usecases
            $usecaseModel = new Usecase();
            $trigger->setUsecases($usecaseModel->fetchByTriggerId($trigger->id));


            $list[] = $trigger;
        }

        return $list;
    }

    /**
     * @return array
     */
    public function fetchByItem($item) {
        $list = array();

        $db = Db::getInstance();
        $req = $db->prepare(sprintf('SELECT * FROM %s WHERE item = :item',$this->table));
        // the query was prepared, now we replace :id with our actual $id value
        $req->execute(array('item' => $item));

        // we create a list of iPost objects from the database results
        foreach($req->fetchAll() as $result) {
            $trigger = new Trigger();
            $trigger->setId($result['id'])
                ->setItem($result['item']);

            //find usecases
            $usecaseModel = new Usecase();
            $trigger->setUsecases($usecaseModel->fetchByTriggerId($trigger->id));


            $list[] = $trigger;
        }

        return $list;
    }

    /**
     * @param $id
     * @return Trigger
     */
    public function FetchById($id) {
        $result = $this->find($id);
        $trigger = new Trigger();
        $trigger->setId($result['id'])
            ->setItem($result['item']);
        return $trigger;
    }

    public function save(){
        if(!$this->id){
            $trigger = $this->insert(array(
                "item" => $this->item
            ));
        } else {
            $trigger = $this->update(array(
                "id" => $this->id,
                "item" => $this->item,
            ));
        }
        unset($trigger->table);
        return $trigger;
    }
}