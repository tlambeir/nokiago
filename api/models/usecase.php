<?php
require_once("model.php");
require_once("category.php");
class Usecase extends Model{

    /**
     * @var string
     */
    public $table = "usecase";

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $item;

    /**
     * @var Category
     */
    public $category;

    /**
     * @var string
     */
    public $description;

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param string $item
     * @return $this
     */
    public function setItem($item)
    {
        $this->item = $item;
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return $this
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return array
     */
    public function fetchByTriggerId($triggerId) {
        $list = array();
        $db = Db::getInstance();
        $req = $db->prepare('SELECT usecase.* FROM triggers
                                    JOIN mapping ON mapping.triggerid = triggers.id
                                    JOIN usecase ON mapping.usecaseid = usecase.id
                                    WHERE triggers.id = :id');
        // the query was prepared, now we replace :id with our actual $id value
        $req->execute(array('id' => $triggerId));

        foreach($req->fetchAll() as $result) {
            $categoryModel = new Category();
            $usecase = new Usecase();
            $usecase->setId($result['id'])
                ->setItem($result['item'])
                ->setDescription($result['description'])
                ->setCategory($categoryModel->FetchById($result['categoryid']));
            $list[] = $usecase;
        }

        return $list;
    }

    /**
     * @return array
     */
    public function fetchAll() {
        $list = array();
        foreach($this->all() as $result) {
            $usecase = new Usecase();
            $usecase->setId($result['id'])
                ->setItem($result['item'])
                ->setDescription($result['description']);
            $list[] = $usecase;
        }

        return $list;
    }

    /**
     * @param $id
     * @return Usecase
     */
    public function FetchById($id) {
        $result = $this->find($id);
        $usecase = new Usecase();
        $usecase->setId($result['id'])
            ->setItem($result['item'])
            ->setDescription($result['description']);
        return $usecase;
    }

    public function save(){
        if(!$this->id){
            $usecase = $this->insert(array(
                "item" => $this->item,
                "description" => $this->description
            ));
        } else {
            $usecase = $this->update(array(
                "id" => $this->id,
                "item" => $this->item,
                "description" => $this->description
            ));
        }
        unset($usecase->table);
        return $usecase;
    }
}