<?php
  function call($controller, $action, $actionWith) {
    require_once('controllers/' . $controller . 'controller.php');

    switch($controller) {
        case 'image':
            $controller = new ImageController($actionWith);
            break;
      break;
      case 'triggers':
        // we need the model to query the database later in the controller
        require_once('models/trigger.php');
        $controller = new TriggersController($actionWith);
      break;
    }

    $controller->{ $action }($actionWith);
  }

  // we're adding an entry for the new controller and its actions
  $controllers = array('triggers' => ['GET', 'PUT','DELETE','POST','error'],
                       'image' => ['POST','error']);

  if (array_key_exists($controller, $controllers)) {
    if (in_array($action, $controllers[$controller])) {

      call($controller, $action, $actionWith);
    } else {
      call('triggers', 'error', $actionWith);
    }
  } else {
    call('triggers', 'error', $actionWith);
  }
?>