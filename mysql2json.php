<?php
/**
 * Created by PhpStorm.
 * User: mnebelsi
 * Date: 8/1/16
 * Time: 12:32 PM
 */

//PDO is a extension which  defines a lightweight, consistent interface for accessing databases in PHP.
//$db=new PDO('mysql:dbname=$dbname;host=$dbhost;','$dbuser','$dbpass');
$db=new PDO('mysql:dbname=iot;host=localhost;','aredu','aredu20!6',array(PDO::MYSQL_ATTR_INIT_COMMAND=>'SET NAMES utf8'));
//here prepare the query for analyzing, prepared statements use less resources and thus run faster
//$row=$db->prepare('select category, usecase, trigger, description from case_mappings');
$row=$db->prepare('select * from case_mappings');

$row->execute();//execute the query
$json_data=array();//create the array
$json_array=array();//create the array
foreach($row as $rec)//foreach loop
{
    $json_array['category']=$rec['category'];
    $json_array['usecase']=$rec['usecase'];
    $json_array['trigger']=$rec['trigger'];
    $json_array['description']=$rec['description'];
//here pushing the values in to an array
    $json_data[]=$json_array;

}
//var_dump($json_data);
//built in PHP function to encode the data in to JSON format
$str = json_encode($json_data);
echo $str;
    //write to json file
    $fp = fopen('usecases.json', 'w');
    fwrite($fp, $str);
    fclose($fp);

?> 
