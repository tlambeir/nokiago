var require = meteorInstall({"imports":{"components":{"error":{"error.html":function(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                //
// imports/components/error/error.html                                                                            //
//                                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                  //
                                                                                                                  // 1
      if (Meteor.isServer) return;                                                                                // 2
                                                                                                                  // 3
      var templateUrl = "/imports/components/error/error.html";                                                   // 4
      var template = "<div ng-init=\"$ctrl.init()\" id=\"error\" class=\"error-wrapper\"> <img ng-if=\"!$ctrl.faded\" class=\"fade img-info\" ng-src=\"./images/{{$ctrl.error.icon}}\"> <span ng-if=\"!$ctrl.faded\" class=\"fade oops\">Oops..</span> <span ng-if=\"!$ctrl.faded\" class=\"fade message\" ng-bind-html=\"$ctrl.error.message\"></span> <span ng-if=\"!$ctrl.faded\" ng-click=\"$ctrl.continue()\" class=\"fade loading-text-error\">Continue</span> <img ng-if=\"!$ctrl.faded\" ng-click=\"$ctrl.continue()\" class=\"fade btn-shutter-error\" src=\"./images/Shutter_Inner.png\"> </div>";
                                                                                                                  // 6
      angular.module('angular-templates')                                                                         // 7
        .run(['$templateCache', function($templateCache) {                                                        // 8
          $templateCache.put(templateUrl, template);                                                              // 9
        }]);                                                                                                      // 10
                                                                                                                  // 11
      module.exports = {};                                                                                        // 12
      module.exports.__esModule = true;                                                                           // 13
      module.exports.default = templateUrl;                                                                       // 14
                                                                                                                  // 15
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"error.js":["babel-runtime/helpers/classCallCheck","angular","angular-meteor","./error.html","angular-animate","angular-sanitize",function(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                //
// imports/components/error/error.js                                                                              //
//                                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                  //
var _classCallCheck;module.import('babel-runtime/helpers/classCallCheck',{"default":function(v){_classCallCheck=v}});var angular;module.import('angular',{"default":function(v){angular=v}});var angularMeteor;module.import('angular-meteor',{"default":function(v){angularMeteor=v}});var template;module.import('./error.html',{"default":function(v){template=v}});var ngAnimate;module.import('angular-animate',{"default":function(v){ngAnimate=v}});var ngSanitize;module.import('angular-sanitize',{"default":function(v){ngSanitize=v}});
                                                                                                                  // 1
                                                                                                                  // 2
                                                                                                                  // 3
                                                                                                                  // 4
                                                                                                                  // 5
                                                                                                                  //
var errorCtrl = function () {                                                                                     //
    function errorCtrl($scope, $element) {                                                                        // 9
        _classCallCheck(this, errorCtrl);                                                                         // 9
                                                                                                                  //
        this.element = $element;                                                                                  // 10
        this.faded = true;                                                                                        // 11
    }                                                                                                             // 12
                                                                                                                  //
    errorCtrl.prototype.init = function init() {                                                                  //
        var resisableElements = this.element.find('.error-wrapper');                                              // 14
        var headerElement = this.element.siblings('.header');                                                     // 15
        resisableElements.height(4 / 3 * window.innerWidth);                                                      // 16
        resisableElements.css({ top: headerElement.outerHeight() });                                              // 17
        this.faded = false;                                                                                       // 18
    };                                                                                                            // 19
                                                                                                                  //
    errorCtrl.prototype['continue'] = function _continue() {                                                      //
        this.faded = true;                                                                                        // 21
        setTimeout(function () {                                                                                  // 22
            this.callBack();                                                                                      // 23
        }.bind(this), 300);                                                                                       // 24
    };                                                                                                            // 25
                                                                                                                  //
    return errorCtrl;                                                                                             //
}();                                                                                                              //
                                                                                                                  //
module.export("default",exports.default=(angular.module('error', [angularMeteor, ngAnimate, ngSanitize]).component('error', {
    templateUrl: 'imports/components/error/error.html',                                                           // 34
    controller: ['$scope', '$element', errorCtrl],                                                                // 35
    bindings: {                                                                                                   // 36
        error: '=',                                                                                               // 37
        callBack: '&'                                                                                             // 38
    }                                                                                                             // 36
})));                                                                                                             // 33
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]},"footer":{"footer.html":function(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                //
// imports/components/footer/footer.html                                                                          //
//                                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                  //
                                                                                                                  // 1
      if (Meteor.isServer) return;                                                                                // 2
                                                                                                                  // 3
      var templateUrl = "/imports/components/footer/footer.html";                                                 // 4
      var template = "<div ng-init=\"$ctrl.init()\" id=\"footer\" class=\"footer\"> <div class=\"carousel\" ng-swipe-left=\"$ctrl.setIndex($ctrl.index+1)\" ng-swipe-right=\"$ctrl.setIndex($ctrl.index-1)\"> <div ng-hide=\"!$ctrl.images[$ctrl.index]\" class=\"img-wrapper\" ng-click=\"$ctrl.setSelectedImage($ctrl.images[$ctrl.index])\" ng-style=\"{'background-image': 'url('+$ctrl.images[$ctrl.index].base64+')'}\"> </div> <div ng-hide=\"!$ctrl.images[$ctrl.index+1]\" class=\"img-wrapper\" ng-click=\"$ctrl.setSelectedImage($ctrl.images[$ctrl.index+1])\" ng-style=\"{'background-image': 'url('+$ctrl.images[$ctrl.index+1].base64+')'}\"> </div> <div ng-hide=\"!$ctrl.images[$ctrl.index+2]\" class=\"img-wrapper\" ng-click=\"$ctrl.setSelectedImage($ctrl.images[$ctrl.index+2])\" ng-style=\"{'background-image': 'url('+$ctrl.images[$ctrl.index+2].base64+')'}\"> </div> <div ng-hide=\"!$ctrl.images[$ctrl.index+3]\" class=\"img-wrapper\" ng-click=\"$ctrl.setSelectedImage($ctrl.images[$ctrl.index+3])\" ng-style=\"{'background-image': 'url('+$ctrl.images[$ctrl.index+3].base64+')'}\"> </div> </div> </div>";
                                                                                                                  // 6
      angular.module('angular-templates')                                                                         // 7
        .run(['$templateCache', function($templateCache) {                                                        // 8
          $templateCache.put(templateUrl, template);                                                              // 9
        }]);                                                                                                      // 10
                                                                                                                  // 11
      module.exports = {};                                                                                        // 12
      module.exports.__esModule = true;                                                                           // 13
      module.exports.default = templateUrl;                                                                       // 14
                                                                                                                  // 15
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"footer.js":["babel-runtime/helpers/classCallCheck","angular","angular-meteor","angular-animate","./footer.html","/imports/services/file/file","angular-touch",function(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                //
// imports/components/footer/footer.js                                                                            //
//                                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                  //
var _classCallCheck;module.import('babel-runtime/helpers/classCallCheck',{"default":function(v){_classCallCheck=v}});var angular;module.import('angular',{"default":function(v){angular=v}});var angularMeteor;module.import('angular-meteor',{"default":function(v){angularMeteor=v}});var ngAnimate;module.import('angular-animate',{"default":function(v){ngAnimate=v}});var template;module.import('./footer.html',{"default":function(v){template=v}});var file;module.import('/imports/services/file/file',{"default":function(v){file=v}});var angularTouch;module.import('angular-touch',{"default":function(v){angularTouch=v}});
                                                                                                                  // 1
                                                                                                                  // 2
                                                                                                                  // 3
                                                                                                                  // 4
                                                                                                                  // 5
                                                                                                                  // 6
                                                                                                                  //
var footerCtrl = function () {                                                                                    //
    function footerCtrl($scope, $element, $q, fileService) {                                                      // 10
        _classCallCheck(this, footerCtrl);                                                                        // 10
                                                                                                                  //
        $scope.viewModel(this);                                                                                   // 11
        this.element = $element;                                                                                  // 12
        this.images;                                                                                              // 13
        this.$q = $q;                                                                                             // 14
        this.fileService = fileService;                                                                           // 15
        this.index = 0;                                                                                           // 16
        this.faded = true;                                                                                        // 17
    }                                                                                                             // 18
                                                                                                                  //
    footerCtrl.prototype.init = function init() {                                                                 //
        var headerElement = this.element.siblings('.header');                                                     // 20
        var footer = this.element.find('.footer');                                                                // 21
        footer.height(headerElement.outerHeight());                                                               // 22
        var carousel = this.element.find('.carousel');                                                            // 23
        carousel.height(footer.height());                                                                         // 24
        var imageWrappers = carousel.find('.img-wrapper');                                                        // 25
        imageWrappers.height(carousel.height() * 0.8);                                                            // 26
        imageWrappers.width(carousel.height() * 0.8);                                                             // 27
        var marginTop = carousel.height() * 0.1;                                                                  // 28
        imageWrappers.css('margin-top', marginTop);                                                               // 29
        var marginLeft = carousel.width() - 4 * carousel.height() * 0.8;                                          // 30
        marginLeft = marginLeft / 5;                                                                              // 31
        imageWrappers.css('margin-left', marginLeft);                                                             // 32
        this.fileService.getImages(this);                                                                         // 33
    };                                                                                                            // 34
                                                                                                                  //
    footerCtrl.prototype.setIndex = function setIndex(index) {                                                    //
        if (index >= 0 && index < this.images.length) {                                                           // 37
            this.faded = false;                                                                                   // 38
            this.index = index;                                                                                   // 39
            setTimeout(function () {                                                                              // 40
                this.faded = true;                                                                                // 41
            }.bind(this), 100);                                                                                   // 42
        }                                                                                                         // 43
    };                                                                                                            // 44
                                                                                                                  //
    footerCtrl.prototype.setSelectedImage = function setSelectedImage(selectedImage) {                            //
        this.selectedImage = selectedImage;                                                                       // 47
        this.fromGallery = true;                                                                                  // 48
    };                                                                                                            // 49
                                                                                                                  //
    return footerCtrl;                                                                                            //
}();                                                                                                              //
                                                                                                                  //
module.export("default",exports.default=(angular.module('footer', [angularMeteor, file.name, angularTouch, ngAnimate]).component('footer', {
    templateUrl: 'imports/components/footer/footer.html',                                                         // 59
    controller: ['$scope', '$element', '$q', 'fileService', footerCtrl],                                          // 60
    bindings: {                                                                                                   // 61
        images: '=',                                                                                              // 62
        selectedImage: '=',                                                                                       // 63
        fromGallery: '='                                                                                          // 64
    }                                                                                                             // 61
})));                                                                                                             // 58
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]},"nokiago":{"nokiago.html":function(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                //
// imports/components/nokiago/nokiago.html                                                                        //
//                                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                  //
                                                                                                                  // 1
      if (Meteor.isServer) return;                                                                                // 2
                                                                                                                  // 3
      var templateUrl = "/imports/components/nokiago/nokiago.html";                                               // 4
      var template = "<div class=\"go-wrapper\" ng-init=\"$ctrl.startVideo();\"> <div class=\"header row\"> <div class=\"info col col-25 text-center\"> <img ng-click=\"$ctrl.showInfo()\" class=\"img-info\" src=\"./images/Info.png\"> </div> <div class=\"logo col col-50 text-center\"> <img class=\"img-logo\" src=\"./images/Logo.png\"> </div> <div class=\"light col col-25 text-center\"> <img ng-if=\"!$ctrl.isSwitchedOn\" ng-click=\"$ctrl.toggleFlashLight()\" class=\"img-info\" src=\"./images/Light_Gray.png\"> <img ng-if=\"$ctrl.isSwitchedOn\" ng-click=\"$ctrl.toggleFlashLight()\" class=\"img-info\" src=\"./images/Light_Orange.png\"> </div> </div> <div ng-if=\"!$ctrl.error && $ctrl.loading\" class=\"backdrop\"></div> <div ng-init=\"$ctrl.resize()\" ng-if=\"!$ctrl.error && $ctrl.loading\" class=\"wrapper-shutter\"> <span class=\"loading-text\">LOADING</span> <img ng-init=\"$ctrl.rotateIcon('#btn-shutter-outer')\" class=\"btn-shutter\" id=\"btn-shutter-outer\" src=\"./images/Shutter_Outer.png\"> </div> <div ng-init=\"$ctrl.resize()\" ng-if=\"!$ctrl.error && !$ctrl.shutterHidden && !$ctrl.Loading && !$ctrl.infoShown\" ng-init=\"$ctrl.resize()\" class=\"fade wrapper-shutter\"> <img ng-init=\"$ctrl.rotateIcon('#btn-shutter')\" class=\"btn-shutter\" id=\"btn-shutter\" src=\"./images/Shutter.svg\" ng-click=\"$ctrl.takePicture()\"> </div> <div ng-init=\"$ctrl.resize()\" ng-if=\"$ctrl.selectedImage && $ctrl.fromGallery\" class=\"fade selected-image\" ng-style=\"{'background-image': 'url('+$ctrl.selectedImage.base64+')'}\"> <div class=\"actions\"> <img ng-click=\"$ctrl.approve()\" src=\"./images/Select.png\"> <img ng-click=\"$ctrl.cancel()\" src=\"./images/GoBack.png\"> <img ng-click=\"$ctrl.delete()\" src=\"./images/Trash.png\"> </div> </div> <div ng-if=\"$ctrl.selectedTrigger\" class=\"backdrop\"></div> <div ng-init=\"$ctrl.resize()\" ng-if=\"$ctrl.selectedTrigger\" class=\"fade trigger\"> <div class=\"trigger-wrapper\"> <div class=\"trigger-header\"> <span class=\"title\">{{$ctrl.selectedTrigger.trigger}}</span> <br> <span>{{$ctrl.percentage}}% certain</span> <br> <br> <span>Choose a category for more information</span> </div> <div class=\"category-wrapper\"> <div class=\"category\" ng-click=\"$ctrl.selectResult(result)\" ng-repeat=\"result in $ctrl.results\"> {{result.category}} </div> </div> <div class=\"continue-wrapper\"> <span class=\"link-button continue\" ng-click=\"$ctrl.cancel()\">Cancel</span> </div> </div> </div> <div ng-if=\"$ctrl.selectedResult\" class=\"backdrop\"></div> <div ng-init=\"$ctrl.resize()\" ng-if=\"$ctrl.selectedResult\" class=\"fade result\"> <div class=\"result-wrapper\"> <span ng-click=\"$ctrl.selectTrigger($ctrl.selectedResult)\" class=\"link-button\">Back</span> <div> <span class=\"title\">{{$ctrl.selectedResult.category}}</span> </div> <div> <p class=\"description\">{{$ctrl.selectedResult.description}}</p> </div> <div class=\"continue-wrapper\"> <span class=\"link-button continue\" ng-click=\"$ctrl.cancel()\">Continue</span> </div> </div> </div> <div ng-if=\"$ctrl.infoShown\" class=\"backdrop\"></div> <div ng-init=\"$ctrl.resize()\" ng-if=\"$ctrl.infoShown\" class=\"fade info2\"> <div class=\"info-wrapper\"> <span class=\"title\">Nokia Go</span> <p class=\"description\">This web-based Augmented Reality pilot app created by the Nokia EDU team to raise awareness about the Internet of Things and our engagement in this field.</p> <p class=\"description\">For more information about the Internet of Things, please visit the following website:</p> <p class=\"description\"><a href=\"https://networks.nokia.com/innovation/iot\">https://networks.nokia.com/innovation/iot</a></p> <div class=\"continue-wrapper\"> <span class=\"info-button continue\" ng-click=\"$ctrl.cancel()\">Continue</span> <img ng-click=\"$ctrl.cancel()\" class=\"fade btn-shutter\" src=\"./images/Shutter_Inner_Blue.png\"> </div> </div> </div> <div ng-if=\"$ctrl.error\" class=\"backdrop\"></div> <error ng-if=\"$ctrl.error\" error=\"$ctrl.error\" call-back=\"$ctrl.cancel()\"></error> <footer from-gallery=\"$ctrl.fromGallery\" images=\"$ctrl.images\" selected-image=\"$ctrl.selectedImage\"></footer> </div>";
                                                                                                                  // 6
      angular.module('angular-templates')                                                                         // 7
        .run(['$templateCache', function($templateCache) {                                                        // 8
          $templateCache.put(templateUrl, template);                                                              // 9
        }]);                                                                                                      // 10
                                                                                                                  // 11
      module.exports = {};                                                                                        // 12
      module.exports.__esModule = true;                                                                           // 13
      module.exports.default = templateUrl;                                                                       // 14
                                                                                                                  // 15
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"nokiago.js":["babel-runtime/helpers/classCallCheck","angular","angular-meteor","angular-animate","./nokiago.html","/imports/services/camera/camera","/imports/services/file/file","/imports/services/data/data","/imports/services/google-vision/google-vision",function(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                //
// imports/components/nokiago/nokiago.js                                                                          //
//                                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                  //
var _classCallCheck;module.import('babel-runtime/helpers/classCallCheck',{"default":function(v){_classCallCheck=v}});var angular;module.import('angular',{"default":function(v){angular=v}});var angularMeteor;module.import('angular-meteor',{"default":function(v){angularMeteor=v}});var ngAnimate;module.import('angular-animate',{"default":function(v){ngAnimate=v}});var template;module.import('./nokiago.html',{"default":function(v){template=v}});var camera;module.import('/imports/services/camera/camera',{"default":function(v){camera=v}});var file;module.import('/imports/services/file/file',{"default":function(v){file=v}});var data;module.import('/imports/services/data/data',{"default":function(v){data=v}});var googleVision;module.import('/imports/services/google-vision/google-vision',{"default":function(v){googleVision=v}});
                                                                                                                  // 1
                                                                                                                  // 2
                                                                                                                  // 3
                                                                                                                  // 4
                                                                                                                  // 5
                                                                                                                  // 6
                                                                                                                  // 7
                                                                                                                  // 8
                                                                                                                  //
var nokiaGoCtrl = function () {                                                                                   //
    function nokiaGoCtrl($scope, $element, cameraService, fileService, dataService, googleVisionService) {        // 11
        _classCallCheck(this, nokiaGoCtrl);                                                                       // 11
                                                                                                                  //
        window.screen.lockOrientation('portrait');                                                                // 12
        $scope.viewModel(this);                                                                                   // 13
        this.startOnPictureEventListener();                                                                       // 14
        this.element = $element;                                                                                  // 15
        this.cameraService = cameraService;                                                                       // 16
        this.fileService = fileService;                                                                           // 17
        this.dataService = dataService;                                                                           // 18
        this.googleVisionService = googleVisionService;                                                           // 19
        this.percentage;                                                                                          // 20
        this.images;                                                                                              // 21
        this.selectedImage;                                                                                       // 22
        this.results;                                                                                             // 23
        this.isSwitchedOn;                                                                                        // 24
        this.selectedResult;                                                                                      // 25
        this.selectedTrigger;                                                                                     // 26
        this.fromGallery = false;                                                                                 // 27
        this.infoShown = false;                                                                                   // 28
    }                                                                                                             // 29
                                                                                                                  //
    nokiaGoCtrl.prototype.startOnPictureEventListener = function startOnPictureEventListener() {                  //
        cordova.plugins.camerapreview.setOnPictureTakenHandler(function (result) {                                // 31
            this.fromGallery = false;                                                                             // 32
            this.loading = true;                                                                                  // 33
            this.shutterHidden = true;                                                                            // 34
            if (device.platform == "Android") {                                                                   // 35
                this.fileService.getBase64fileFromPath(this, 'file://' + result[0]);                              // 36
            } else if (device.platform == "iOS") {                                                                // 37
                this.fileService.insert(result[1]);                                                               // 38
                this.fileService.getBase64fileFromPath(this, result[1]);                                          // 39
            }                                                                                                     // 40
            this.fileService.getImages(this);                                                                     // 41
        }.bind(this));                                                                                            // 42
    };                                                                                                            // 43
                                                                                                                  //
    nokiaGoCtrl.prototype.toggleFlashLight = function toggleFlashLight() {                                        //
        window.plugins.flashlight.available(function (isAvailable) {                                              // 46
            if (isAvailable) {                                                                                    // 47
                window.plugins.flashlight.toggle();                                                               // 48
            } else {                                                                                              // 49
                alert("Flashlight not available on this device");                                                 // 50
            }                                                                                                     // 51
        });                                                                                                       // 52
    };                                                                                                            // 53
                                                                                                                  //
    nokiaGoCtrl.prototype.startVideo = function startVideo() {                                                    //
        var headerElement = this.element.find('.header');                                                         // 56
        this.cameraService.startVideo(false, false, true, { x: 0, y: headerElement.outerHeight(), width: window.innerWidth, height: window.innerHeight - 2 * headerElement.outerHeight() });
    };                                                                                                            // 58
                                                                                                                  //
    nokiaGoCtrl.prototype.takePicture = function takePicture() {                                                  //
        this.cameraService.takePicture();                                                                         // 61
    };                                                                                                            // 62
                                                                                                                  //
    nokiaGoCtrl.prototype.resize = function resize() {                                                            //
        var resisableElements = this.element.find('.selected-image, .button-go-wrapper, .info2, .result, .trigger, .wrapper-shutter');
        var headerElement = this.element.find('.header');                                                         // 66
        resisableElements.height(window.innerHeight - 2 * headerElement.outerHeight());                           // 67
        resisableElements.css({ top: headerElement.outerHeight() });                                              // 68
    };                                                                                                            // 69
                                                                                                                  //
    nokiaGoCtrl.prototype['delete'] = function _delete() {                                                        //
        this.fileService['delete'](this.selectedImage.url).then(function () {                                     // 72
            console.log(this.images);                                                                             // 73
            for (var i = 0; i < this.images.length; i++) {                                                        // 74
                if (this.images[i].base64 == this.selectedImage.base64) {                                         // 75
                    this.images.splice(i, 1);                                                                     // 76
                }                                                                                                 // 77
            }                                                                                                     // 78
            console.log(this.images);                                                                             // 79
            this.cancel();                                                                                        // 80
        }.bind(this));                                                                                            // 81
    };                                                                                                            // 82
                                                                                                                  //
    nokiaGoCtrl.prototype.approve = function approve() {                                                          //
        // get response from cloudvision                                                                          //
        this.loading = true;                                                                                      // 86
        this.shutterHidden = true;                                                                                // 87
        var base64 = angular.copy(this.selectedImage.base64);                                                     // 88
        delete this.selectedImage;                                                                                // 89
        this.googleVisionService.detectLabel(base64).then(function (response) {                                   // 90
            var trigger = response.data.responses[0].labelAnnotations[0].description;                             // 91
            if (response.data.responses.length > 0) {                                                             // 92
                this.percentage = response.data.responses[0].labelAnnotations[0].score * 100;                     // 93
                this.dataService.find(trigger).then(function (results) {                                          // 94
                    if (results.length > 0) {                                                                     // 95
                        this.results = results;                                                                   // 96
                        this.selectedTrigger = this.results[0];                                                   // 97
                        this.loading = false;                                                                     // 98
                    } else {                                                                                      // 99
                        this.error = {                                                                            // 100
                            icon: 'Error_Database.png',                                                           // 101
                            message: 'This is probably a ' + response.data.responses[0].labelAnnotations[0].description + '..<br>But it\'s not in our IOT database yet!<br>Please continue to check another object!'
                        };                                                                                        // 100
                    }                                                                                             // 104
                }.bind(this));                                                                                    // 105
            } else {                                                                                              // 106
                this.error = {                                                                                    // 107
                    icon: 'Error_Image.png',                                                                      // 108
                    message: 'We could not recognize this image..<br>Please continue to check another object!'    // 109
                };                                                                                                // 107
            }                                                                                                     // 111
        }.bind(this))['catch'](function (error) {                                                                 // 112
            console.log(error);                                                                                   // 114
            this.error = {                                                                                        // 115
                icon: 'Error_Connection.png',                                                                     // 116
                message: 'There seems to be something wrong..<br>Please check your connection and continue!'      // 117
            };                                                                                                    // 115
        }.bind(this));                                                                                            // 119
    };                                                                                                            // 121
                                                                                                                  //
    nokiaGoCtrl.prototype.selectTrigger = function selectTrigger(trigger) {                                       //
        this.selectedTrigger = trigger;                                                                           // 123
        delete this.selectedResult;                                                                               // 124
    };                                                                                                            // 125
                                                                                                                  //
    nokiaGoCtrl.prototype.selectResult = function selectResult(result) {                                          //
        this.selectedResult = result;                                                                             // 127
        delete this.selectedTrigger;                                                                              // 128
    };                                                                                                            // 129
                                                                                                                  //
    nokiaGoCtrl.prototype.cancel = function cancel() {                                                            //
        delete this.selectedImage;                                                                                // 131
        delete this.results;                                                                                      // 132
        delete this.selectedResult;                                                                               // 133
        delete this.selectedTrigger;                                                                              // 134
        delete this.error;                                                                                        // 135
        this.shutterHidden = false;                                                                               // 136
        this.infoShown = false;                                                                                   // 137
        this.loading = false;                                                                                     // 138
    };                                                                                                            // 139
                                                                                                                  //
    nokiaGoCtrl.prototype.showInfo = function showInfo() {                                                        //
        this.infoShown = true;                                                                                    // 142
    };                                                                                                            // 143
                                                                                                                  //
    nokiaGoCtrl.prototype.rotateIcon = function rotateIcon(elementID) {                                           //
        var $rota = $(elementID),                                                                                 // 146
            degree = 0,                                                                                           // 146
            timer,                                                                                                // 146
            angle = 5,                                                                                            // 146
            delay = 41,                                                                                           // 146
            acceleration = 0.15,                                                                                  // 146
            maxAngle = 180;                                                                                       // 146
        function spinRight() {                                                                                    // 153
            $rota.css({ transform: 'rotate(' + degree + 'deg)' });                                                // 154
            timer = setTimeout(function () {                                                                      // 155
                angle += acceleration;                                                                            // 156
                degree += angle;                                                                                  // 157
                if (degree < maxAngle) {                                                                          // 158
                    spinRight(); // loop it                                                                       // 159
                } else {                                                                                          // 160
                        angle = 5;                                                                                // 161
                        maxAngle = 180 + Math.floor(Math.random() * 4) * 30;                                      // 162
                        spinLeft();                                                                               // 163
                    }                                                                                             // 164
            }, delay);                                                                                            // 165
        }                                                                                                         // 166
        function spinLeft() {                                                                                     // 167
            $rota.css({ transform: 'rotate(' + degree + 'deg)' });                                                // 168
            timer = setTimeout(function () {                                                                      // 169
                angle += acceleration;                                                                            // 170
                degree -= angle;                                                                                  // 171
                if (degree > 0) {                                                                                 // 172
                    spinLeft();                                                                                   // 173
                } else {                                                                                          // 174
                    angle = 5;                                                                                    // 175
                    spinRight(); // loop it                                                                       // 176
                }                                                                                                 // 177
            }, delay);                                                                                            // 178
        }                                                                                                         // 179
        spinRight(); // run it!                                                                                   // 180
    };                                                                                                            // 181
                                                                                                                  //
    return nokiaGoCtrl;                                                                                           //
}();                                                                                                              //
                                                                                                                  //
module.export("default",exports.default=(angular.module('nokiaGo', [angularMeteor, camera.name, file.name, data.name, googleVision.name, ngAnimate]).component('nokiaGo', {
    templateUrl: 'imports/components/nokiago/nokiago.html',                                                       // 193
    controller: ['$scope', '$element', 'cameraService', 'fileService', 'dataService', 'googleVisionService', nokiaGoCtrl]
})));                                                                                                             // 192
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]},"test":{"test.html":function(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                //
// imports/components/test/test.html                                                                              //
//                                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                  //
                                                                                                                  // 1
      if (Meteor.isServer) return;                                                                                // 2
                                                                                                                  // 3
      var templateUrl = "/imports/components/test/test.html";                                                     // 4
      var template = "<div> {{$ctrl.test[0]}} </div>";                                                            // 5
                                                                                                                  // 6
      angular.module('angular-templates')                                                                         // 7
        .run(['$templateCache', function($templateCache) {                                                        // 8
          $templateCache.put(templateUrl, template);                                                              // 9
        }]);                                                                                                      // 10
                                                                                                                  // 11
      module.exports = {};                                                                                        // 12
      module.exports.__esModule = true;                                                                           // 13
      module.exports.default = templateUrl;                                                                       // 14
                                                                                                                  // 15
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"test.js":["babel-runtime/helpers/classCallCheck","angular","angular-meteor","./test.html","/imports/services/camera/camera",function(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                //
// imports/components/test/test.js                                                                                //
//                                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                  //
var _classCallCheck;module.import('babel-runtime/helpers/classCallCheck',{"default":function(v){_classCallCheck=v}});var angular;module.import('angular',{"default":function(v){angular=v}});var angularMeteor;module.import('angular-meteor',{"default":function(v){angularMeteor=v}});var template;module.import('./test.html',{"default":function(v){template=v}});var camera;module.import('/imports/services/camera/camera',{"default":function(v){camera=v}});
                                                                                                                  // 1
                                                                                                                  // 2
                                                                                                                  // 3
                                                                                                                  // 4
                                                                                                                  //
var testCtrl = function testCtrl($scope, $element, cameraService) {                                               //
    _classCallCheck(this, testCtrl);                                                                              // 7
                                                                                                                  //
    this.test = "een zin";                                                                                        // 8
    this.test = "een andere zin";                                                                                 // 9
    cameraService.doSomething();                                                                                  // 10
};                                                                                                                // 11
                                                                                                                  //
module.export("default",exports.default=(angular.module('test', [angularMeteor, camera.name]).component('test', {
    templateUrl: 'imports/components/test/test.html',                                                             // 19
    controller: ['$scope', '$element', 'cameraService', testCtrl]                                                 // 20
})));                                                                                                             // 18
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]}},"services":{"camera":{"camera.js":["angular","angular-meteor",function(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                //
// imports/services/camera/camera.js                                                                              //
//                                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                  //
var angular;module.import('angular',{"default":function(v){angular=v}});var angularMeteor;module.import('angular-meteor',{"default":function(v){angularMeteor=v}});
                                                                                                                  // 2
                                                                                                                  //
module.export("default",exports.default=(angular.module('camera', [angularMeteor]).service('cameraService', function () {
    this.startVideo = function (tapEnabled, dragEnabled, toBack, rect) {                                          // 8
        var tapEnabled = tapEnabled; //enable tap take picture                                                    // 9
        var dragEnabled = dragEnabled; //enable preview box drag across the screen                                // 10
        var toBack = toBack; //send preview box to the back of the webview                                        // 11
        var rect = rect;                                                                                          // 12
        cordova.plugins.camerapreview.startCamera(rect, "rear", tapEnabled, dragEnabled, toBack);                 // 13
    };                                                                                                            // 14
                                                                                                                  //
    this.takePicture = function (options) {                                                                       // 16
        cordova.plugins.camerapreview.takePicture(options);                                                       // 17
    };                                                                                                            // 18
})));                                                                                                             // 19
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]},"data":{"data.js":["angular","angular-meteor",function(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                //
// imports/services/data/data.js                                                                                  //
//                                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                  //
var angular;module.import('angular',{"default":function(v){angular=v}});var angularMeteor;module.import('angular-meteor',{"default":function(v){angularMeteor=v}});
                                                                                                                  // 2
                                                                                                                  //
module.export("default",exports.default=(angular.module('data', [angularMeteor]).service('dataService', ['$http', function ($http) {
    this.find = function (trigger) {                                                                              // 8
        return $http({                                                                                            // 9
            method: 'GET',                                                                                        // 10
            url: '/data/usecases.json'                                                                            // 11
        }).then(function successCallback(response) {                                                              // 9
            var results = [];                                                                                     // 13
            for (i = 0; i < response.data.length; i++) {                                                          // 14
                if (response.data[i].trigger == trigger) {                                                        // 15
                    results.push(response.data[i]);                                                               // 16
                }                                                                                                 // 17
            }                                                                                                     // 18
            return results;                                                                                       // 19
        }, function errorCallback(response) {                                                                     // 20
            console.log(response);                                                                                // 21
        });                                                                                                       // 22
    };                                                                                                            // 23
}])));                                                                                                            // 24
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]},"file":{"file.js":["angular","angular-meteor",function(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                //
// imports/services/file/file.js                                                                                  //
//                                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                  //
var angular;module.import('angular',{"default":function(v){angular=v}});var angularMeteor;module.import('angular-meteor',{"default":function(v){angularMeteor=v}});
                                                                                                                  // 2
                                                                                                                  //
module.export("default",exports.default=(angular.module('file', [angularMeteor]).service('fileService', ['$q', function ($q) {
    this['delete'] = function (path) {                                                                            // 8
        var filename = path.split('/').pop();                                                                     // 9
                                                                                                                  //
        var deferred = $q.defer();                                                                                // 11
                                                                                                                  //
        resolveLocalFileSystemURL(cordova.file.dataDirectory + filename, function (entry) {                       // 13
            entry.remove(function (entry) {                                                                       // 14
                deferred.resolve(entry);                                                                          // 15
            }, function (error) {                                                                                 // 16
                deferred.reject(error);                                                                           // 18
            });                                                                                                   // 19
        });                                                                                                       // 20
        return deferred.promise;                                                                                  // 21
    };                                                                                                            // 22
    this.getBase64fileFromPath = function (context, path) {                                                       // 23
        var deferred = $q.defer();                                                                                // 24
        resolveLocalFileSystemURL(path, function (fileEntry) {                                                    // 25
            // no callback                                                                                        //
            fileEntry.file(function (file) {                                                                      // 27
                var reader = new FileReader();                                                                    // 28
                reader.onloadend = function (event) {                                                             // 29
                    var base64 = event.target.result;                                                             // 30
                    context.selectedImage = {                                                                     // 31
                        base64: base64                                                                            // 32
                    };                                                                                            // 31
                    context.approve();                                                                            // 34
                    deferred.resolve($q.all());                                                                   // 35
                };                                                                                                // 36
                reader.readAsDataURL(file);                                                                       // 37
            }, function (error) {                                                                                 // 38
                console.log('fileEntry error');                                                                   // 39
                console.log(error);                                                                               // 40
            });                                                                                                   // 41
        }, function (error) {                                                                                     // 42
            console.log('resolveLocalFileSystemURL error');                                                       // 43
            console.log(error);                                                                                   // 44
        });                                                                                                       // 45
    };                                                                                                            // 46
                                                                                                                  //
    this.getImages = function (context) {                                                                         // 48
        var deferred = $q.defer();                                                                                // 49
        if (device.platform == "Android") {                                                                       // 50
            resolveLocalFileSystemURL(cordova.file.dataDirectory, function (fileSystem) {                         // 51
                console.log('in fileSystem');                                                                     // 53
                var reader = fileSystem.createReader();                                                           // 54
                reader.readEntries(function (entries) {                                                           // 55
                    console.log('in readEntries');                                                                // 57
                    var images = [];                                                                              // 58
                    var promises = [];                                                                            // 59
                    for (i = entries.length - 1; i >= 0; i--) {                                                   // 60
                        var row = entries[i];                                                                     // 61
                        if (!row.isDirectory && row.nativeURL.split('_').pop() == 'original.jpg') {               // 62
                            var deferred2 = $q.defer();                                                           // 63
                            // We will draw the content of the clicked folder                                     //
                            entries[i].file(function (file) {                                                     // 65
                                var reader = new FileReader();                                                    // 66
                                reader.onloadend = function (event) {                                             // 67
                                    var image = {                                                                 // 68
                                        url: event.target._localURL,                                              // 69
                                        base64: event.target.result                                               // 70
                                    };                                                                            // 68
                                    images.push(image);                                                           // 72
                                    deferred2.resolve(image);                                                     // 73
                                };                                                                                // 74
                                reader.readAsDataURL(file);                                                       // 75
                            }, function (error) {                                                                 // 76
                                console.log('fileEntry error');                                                   // 77
                                console.log(error);                                                               // 78
                            });                                                                                   // 79
                            promises.push(deferred2.promise);                                                     // 80
                        }                                                                                         // 81
                    }                                                                                             // 82
                    deferred.resolve($q.all(promises));                                                           // 83
                    context.images = images;                                                                      // 84
                }.bind(context), function (err) {                                                                 // 85
                    console.log(err);                                                                             // 87
                });                                                                                               // 88
            }.bind(context), function (err) {                                                                     // 90
                console.log(err);                                                                                 // 91
            });                                                                                                   // 92
        } else if (device.platform == "iOS") {                                                                    // 94
            var storage = window.localStorage;                                                                    // 95
            var assetLinks = angular.fromJson(storage.getItem("photo")).reverse();                                // 96
            var images = [];                                                                                      // 97
            var promises = [];                                                                                    // 98
            angular.forEach(assetLinks, function (assetLink) {                                                    // 99
                var deferred2 = $q.defer();                                                                       // 100
                resolveLocalFileSystemURL(assetLink, function (fileEntry) {                                       // 101
                    // no callback                                                                                //
                    fileEntry.file(function (file) {                                                              // 103
                        var reader = new FileReader();                                                            // 104
                        reader.onloadend = function (event) {                                                     // 105
                            var base64 = event.target.result;                                                     // 106
                            deferred2.resolve();                                                                  // 107
                            images.push(base64);                                                                  // 108
                        };                                                                                        // 109
                        reader.readAsDataURL(file);                                                               // 110
                    }, function (error) {                                                                         // 111
                        console.log('fileEntry error');                                                           // 112
                        console.log(error);                                                                       // 113
                    });                                                                                           // 114
                }, function (error) {                                                                             // 115
                    console.log('resolveLocalFileSystemURL error');                                               // 116
                    console.log(error);                                                                           // 117
                });                                                                                               // 118
                promises.push(deferred2.promise);                                                                 // 119
            });                                                                                                   // 120
            deferred.resolve($q.all(promises));                                                                   // 121
            context.images = images;                                                                              // 122
            console.log("images count: " + context.images.length);                                                // 123
        }                                                                                                         // 124
                                                                                                                  //
        return deferred.promise;                                                                                  // 126
    };                                                                                                            // 127
    this.insert = function (assetLink) {                                                                          // 128
        var storage = window.localStorage;                                                                        // 129
        var value = storage.getItem("photo");                                                                     // 130
        if (value == undefined) {                                                                                 // 131
            value = [];                                                                                           // 132
        } else {                                                                                                  // 133
            value = angular.fromJson(value);                                                                      // 134
        }                                                                                                         // 135
        value.push(assetLink);                                                                                    // 136
        storage.setItem("photo", angular.toJson(value));                                                          // 137
                                                                                                                  //
        console.log("stored, new size: " + angular.fromJson(value).length);                                       // 139
    };                                                                                                            // 140
}])));                                                                                                            // 142
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]},"google-vision":{"google-vision.js":["angular","angular-meteor",function(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                //
// imports/services/google-vision/google-vision.js                                                                //
//                                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                  //
var angular;module.import('angular',{"default":function(v){angular=v}});var angularMeteor;module.import('angular-meteor',{"default":function(v){angularMeteor=v}});
                                                                                                                  // 2
                                                                                                                  //
module.export("default",exports.default=(angular.module('googleVision', [angularMeteor]).service('googleVisionService', ['$http', function ($http) {
                                                                                                                  //
	var keys = {                                                                                                     // 8
		android: 'AIzaSyDDiSHWLP-KnrBpL3uN9jJzzqmYce-XyBU',                                                             // 9
		ios: 'AIzaSyD2vlyMyUBuw4Ulc5bicGGZahBoAISpSGw'                                                                  // 10
	};                                                                                                               // 8
                                                                                                                  //
	this.detectLabel = function (base64Content) {                                                                    // 14
		var url = 'https://vision.googleapis.com/v1/images:annotate?key=' + (device.platform == "Android" ? keys.android : keys.ios);
		//                                                                                                              //
		var postData = angular.toJson({                                                                                 // 17
			requests: [{                                                                                                   // 18
				image: {                                                                                                      // 20
					content: base64Content.replace('data:image/jpeg;base64,', '')                                                // 21
				},                                                                                                            // 20
				features: [{                                                                                                  // 23
					type: "LABEL_DETECTION",                                                                                     // 25
					maxResults: 5                                                                                                // 26
				}]                                                                                                            // 24
			}]                                                                                                             // 19
		}, true);                                                                                                       // 17
                                                                                                                  //
		return $http({                                                                                                  // 33
			method: 'POST',                                                                                                // 34
			url: url,                                                                                                      // 35
			data: postData,                                                                                                // 36
			headers: { 'Content-Type': 'application/json' }                                                                // 37
		})['catch'](function (error) {                                                                                  // 33
			return 'Unable to connect to cloudvision';                                                                     // 39
		});                                                                                                             // 40
	};                                                                                                               // 41
}])));                                                                                                            // 42
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]}}},"client":{"main.html.js":function(){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                //
// client/main.html.js                                                                                            //
//                                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                  //
                                                                                                                  // 1
            Meteor.startup(function() {                                                                           // 2
              var attrs = {};                                                                                     // 3
              for (var prop in attrs) {                                                                           // 4
                document.body.setAttribute(prop, attrs[prop]);                                                    // 5
              }                                                                                                   // 6
            });                                                                                                   // 7
                                                                                                                  // 8
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"main.js":["angular","angular-meteor","../imports/components/nokiago/nokiago","../imports/components/footer/footer","../imports/components/error/error","../imports/components/test/test",function(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                //
// client/main.js                                                                                                 //
//                                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                  //
var angular;module.import('angular',{"default":function(v){angular=v}});var angularMeteor;module.import('angular-meteor',{"default":function(v){angularMeteor=v}});var nokiaGo;module.import('../imports/components/nokiago/nokiago',{"default":function(v){nokiaGo=v}});var footer;module.import('../imports/components/footer/footer',{"default":function(v){footer=v}});var error;module.import('../imports/components/error/error',{"default":function(v){error=v}});var test;module.import('../imports/components/test/test',{"default":function(v){test=v}});
                                                                                                                  // 2
                                                                                                                  // 3
                                                                                                                  // 4
                                                                                                                  // 5
                                                                                                                  // 6
                                                                                                                  //
angular.module('nokia-go-app', [angularMeteor, nokiaGo.name, footer.name, error.name, test.name]);                // 8
                                                                                                                  //
function onReady() {                                                                                              // 16
  angular.bootstrap(document, ['nokia-go-app']);                                                                  // 17
  if (window.cordova.logger) {                                                                                    // 18
    window.cordova.logger.__onDeviceReady();                                                                      // 19
  }                                                                                                               // 20
}                                                                                                                 // 21
                                                                                                                  //
var handle = LaunchScreen.hold();                                                                                 // 23
if (Meteor.isCordova) {                                                                                           // 24
  var count;                                                                                                      // 24
  var timer;                                                                                                      // 24
                                                                                                                  //
  (function () {                                                                                                  // 24
    var exitApp = function exitApp() {                                                                            // 24
      navigator.app.exitApp();                                                                                    // 43
    };                                                                                                            // 44
                                                                                                                  //
    angular.element(document).on('deviceready', onReady);                                                         // 25
                                                                                                                  //
    count = 2;                                                                                                    // 27
    timer = setInterval(function () {                                                                             // 28
      count--;                                                                                                    // 29
      console.log('count:', count);                                                                               // 30
      if (count == 0) {                                                                                           // 31
        handle.release();                                                                                         // 32
        clearInterval(timer);                                                                                     // 33
      }                                                                                                           // 34
    }, 1000);                                                                                                     // 35
                                                                                                                  //
                                                                                                                  //
    document.addEventListener("backbutton", function () {                                                         // 37
      // pass exitApp as callbacks to the switchOff method                                                        //
      window.plugins.flashlight.switchOff(exitApp, exitApp);                                                      // 39
    }, false);                                                                                                    // 40
  })();                                                                                                           // 24
} else {                                                                                                          // 46
  angular.element(document).ready(onReady);                                                                       // 47
}                                                                                                                 // 48
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]}},{"extensions":[".js",".json",".html",".css"]});
require("./client/main.html.js");
require("./client/main.js");